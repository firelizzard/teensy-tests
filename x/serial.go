package main

import (
	"bufio"
	"log"

	"github.com/tarm/serial"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lmicroseconds)

	c, err := serial.OpenPort(&serial.Config{Name: "/dev/tty.usbmodem79AD7CA83", Baud: 115200})
	if err != nil {
		log.Fatal(err)
	}

	r := bufio.NewReader(c)
	for {
		s, err := r.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}
		log.Print(s)
	}
}