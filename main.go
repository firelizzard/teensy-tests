package main

import (
	"machine"
	"runtime"
	"time"
)

func main() {
	machine.UART0.Configure(machine.UARTConfig{})
	machine.LED.Configure(machine.PinConfig{Mode: machine.PinOutput})

	machine.D29.Configure(machine.PinConfig{Mode: machine.PinTSI})

	go func() {
		b := []byte{0}
		for {
			n, err := machine.UART0.Read(b)
			if n == 0 {
				runtime.Gosched()
				continue
			}
			if err != nil {
				panic(err)
			}

			if b[0] == '\n' || b[0] == '\r' {
				machine.UART0.WriteByte('\r')
				machine.UART0.WriteByte('\n')
			} else {
				machine.UART0.WriteByte(b[0])
			}
		}
	}()

	for /*range time.Tick(time.Second)*/ {
		machine.LED.Set(true)
		time.Sleep(time.Second / 4)

		machine.LED.Set(false)
		time.Sleep(time.Second * 3 / 4)
	}
}
